<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>signup</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<div style="background-color: rgb(102, 102, 102);">
	<div class="container" >
		<div class="row justify-content-end">
			<div class="col-3"
				style="color: rgb(255, 255, 255); padding: 30; font-size: 30px;">
				${userInfo.name}さん</div>
			<div class="col-3">
				<nav class="pc-nav">
					<ul>
						<a href="Logout" style="color: rgb(156, 47, 47)"; "font-size: 20px";>
						<!-- aタグの書き方、ダブルコーテーションの位置をチェック -->
							ログアウト </a>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>

</head>

<body>

	<form method="post" action="Signup">
		<h1>
			<div class="row justify-content-center" style="padding: 30;">
				ユーザー新規登録</div>
		</h1>
		<div class="container">
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
			<div class="row">
				<div class="col">ログインID</div>
				<div class="col">
					<input type="text" name="inputloginId" class="form-control"
						id="inputloginId" >
				</div>
			</div>
			<div class="row">
				<div class="col">ユーザ名</div>
				<div class="col">
					<input type="text" name="inputName" class="form-control"
						id="inputName" >
				</div>
			</div>
			<div class="row">
				<div class="col">パスワード</div>
				<div class="col">
					<input type="password" name="inputPassword" class="form-control"
						id="inputPassword">
				</div>
			</div>
			<div class="row">
				<div class="col">パスワード(確認)</div>
				<div class="col">
					<input type="password" name="inputPasswordCheck"
						class="form-control" id="inputPasswordCheck">
				</div>
			</div>
			<div class="row">
				<div class="col">生年月日</div>
				<div class="col">
					<input type="date" name="inputBirthDate" id="inputBirthDate"
						class="form-control" size="30" >
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<button class="btn btn-lg btn-primary btn-block btn-signin"
				type="submit">登録</button>
		</div>
		<div class="row">
			<ul>
				<a href="UserListServlet"> 戻る </a>
			</ul>
		</div>
	</form>
</body>

</html>