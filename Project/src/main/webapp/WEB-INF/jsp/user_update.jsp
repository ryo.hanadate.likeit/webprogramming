<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>update</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<div style="background-color: rgb(102, 102, 102);">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-3"
				style="color: rgb(255, 255, 255); padding: 30; font-size: 30px;">
				${userInfo.name} さん</div>
			<div class="col-3">
				<nav class="pc-nav">
					<ul>
						<a href="Logout" style="color: rgb(156, 47, 47); font-size: 20px;">
							ログアウト </a>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
</head>

<body>
	<form method="post" action="Update">
		<h1>
			<div class="row justify-content-center" style="padding: 30;">
				ユーザー情報更新</div>
		</h1>
		<div class="container">
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>
			<div class="row">
				<div class="col">ユーザID</div>
				<div class="col">
					<c:if test="${insertId == null}">
					<input type="text" class="form-control" disabled
						value="${userDetails.id}"> <input type="hidden" name="id"
						id="id" value="${userDetails.id}">
					</c:if>
					<c:if test="${insertId != null}">
					<input type="text" class="form-control" disabled
						value="<c:out value='${insertId[0]}'/>"> <input type="hidden" name="id"
						id="id" value="<c:out value='${insertId[0]}'/>">
					</c:if>
					<!-- disabledだとinputそのものが無効になるため、別で属性値を渡す文を記述 -->
				</div>
			</div>
			<div class="row">
				<div class="col">ユーザ名</div>
				<div class="col">
					<c:if test="${insertName == null}">
						<input type="text" name="updateName" class="form-control"
							id="updateName" value="${userDetails.name}">
					</c:if>
					<c:if test="${insertName != null}">
						<input type="text" name="updateName" class="form-control"
							id="updateName" value="<c:out value='${insertName[0]}'/>">
					</c:if>
				</div>
			</div>
			<div class="row">
				<div class="col">パスワード</div>
				<div class="col">
					<input type="password" name="updatePassword" class="form-control"
						id="updatePassword">
				</div>
			</div>
			<div class="row">
				<div class="col">パスワード(確認)</div>
				<div class="col">
					<input type="password" name="updatePasswordCheck"
						class="form-control" id="updatePasswordCheck">
				</div>
			</div>
			<div class="row">
				<div class="col">生年月日</div>
				<div class="col">
					<c:if test="${insertBirthDate == null}">
						<input type="date" name="updateBirthDate" id="updateBirthDate"
							class="form-control" size="30" value="${userDetails.birthDate}">
					</c:if>
					<c:if test="${insertBirthDate != null}">
						<input type="date" name="updateBirthDate" id="updateBirthDate"
							class="form-control" size="30"
							value="<c:out value='${insertBirthDate[0]}'/>">
						
					</c:if>
				</div>
			</div>


		</div>
		<div class="row justify-content-center">
			<input type="submit" value="    更新    ">
		</div>
		<div class="row">
			<ul>
				<a href="UserListServlet"> 戻る </a>
			</ul>
		</div>
	</form>
</body>

</html>