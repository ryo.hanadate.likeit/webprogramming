<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="date" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<title>info</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>

	<div class="container" style="background-color: rgb(102, 102, 102);">
		<div class="row justify-content-end">
			<div class="col-3"
				style="color: rgb(255, 255, 255); padding: 30; font-size: 30px;">
				${userInfo.name}さん</div>
			<div class="col-3">
				<nav class="pc-nav">
					<ul>
						<a href="Logout" style="color: rgb(156, 47, 47); font-size: 20px;">
							ログアウト </a>
					</ul>
				</nav>
			</div>
		</div>
	</div>

</head>

<body>
	<form>
		<h1>
			<div class="row justify-content-center" style="padding: 30;">
				ユーザー情報詳細参照</div>
			</div>
		</h1>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">項目</th>
						<th scope="col">情報</th>
					</tr>
				</thead>
				<tbody>
					
					<tr>
						<th scope="row">ログインID</th>
						<td>${userDetails.loginId}</td>

					</tr>
					<tr>
						<th scope="row">ユーザ名</th>
						<td>${userDetails.name}</td>

					</tr>
					<tr>
						<th scope="row">生年月日</th>
						<td><fmt:formatDate value="${userDetails.birthDate}" pattern="YYYY年MM月dd日" /></td>

					</tr>
					<tr>
						<th scope="row">登録日時</th>
						<td><fmt:formatDate value="${userDetails.createDate}" pattern="YYYY年MM月dd日 HH:MM" /></td>

					</tr>
					<tr>
						<th scope="row">更新日時</th>
						<td><fmt:formatDate value="${userDetails.updateDate}" pattern="YYYY年MM月dd日 HH:MM" /></td>

					</tr>
					
				</tbody>
			</table>
		</div>
		<div class="row">
			<ul>
				<a href="UserListServlet"> 戻る </a>
			</ul>
		</div>
	</form>
</body>

</html>