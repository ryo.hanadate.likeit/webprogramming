<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>delete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<div style="background-color: rgb(102, 102, 102);">
	<div class="container">
		<div class="row justify-content-end">
			<div class="col-3"
				style="color: rgb(255, 255, 255); padding: 30; font-size: 30px;">
				${userInfo.name} さん</div>
			<div class="col-3">
				<nav class="pc-nav">
					<ul>
						<li class="dropdown"><a href="Logout"
						class="navbar-link logout-link">ログアウト</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
</head>

<body>
	<form method="post" action="Delete">
		<h1>
			<div class="row justify-content-center" style="padding: 30;">
				ユーザー削除確認</div>
		</h1>
		<div class="container">
			<div class="row">

				<div class="col">
					<h2>ユーザ名</h2>
				</div>
				<div class="col">
					<input type="hidden" name="id" class="form-control" id="id"
						value="${userDetails.id}">
				</div>
				<div class="col">
					<h2>${userDetails.name}さん</h2>

				</div>
			</div>
			<div class="row">を本当に削除してよろしいでしょうか。</div>
			<div class="row justify-content-around">
				<a class="btn btn-primary" href="UserListServlet">戻る</a>
				<button type="submit" value="検索" class="btn btn-primary form-submit">削除</button>
			</div>
		</div>
	</form>
</body>

</html>