package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		// ログイン用
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DatabaseManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 *
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DatabaseManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> search(String Id, String Name, String DateStart, String DateEnd) {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DatabaseManager.getConnection();
			String sql = "SELECT * FROM user WHERE id != 1";

			if (!Id.equals("")) {
				sql += " " + "AND login_id = '" + Id + "'";
			}
			if (!Name.equals("")) {
				sql += " " + "AND name LIKE '%" + Name + "%'";
			}
			/*
			 * if (!DateStart.equals("") && !DateEnd.equals("")) { sql += " " +
			 * "AND birth_date >= '" + DateStart + "' AND " + "birth_date <= '" + DateEnd +
			 * "'"; }
			 */
			if (!DateStart.equals("")) {
				sql += " " + "AND birth_date >= '" + DateStart + "'";
			}
			if (!DateEnd.equals("")) {
				sql += " " + "AND birth_date <= '" + DateEnd + "'";
			}

			System.out.println(sql);
			Statement st = conn.createStatement();

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);

			}

		} catch (SQLException e) {

			e.printStackTrace();
			return null;

		} finally {

			if (conn != null) {

				try {
					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();
					return null;

				}
			}
		}

		return userList;

	}

	public User infomation(String Id) {
		Connection conn = null;

		try {

			conn = DatabaseManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {

				return null;

			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = java.sql.Date.valueOf(rs.getString("birth_date"));
			String password = rs.getString("password");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

			return user;

		} catch (SQLException e) {

			e.printStackTrace();
			return null;

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();
					return null;

				}
			}
		}
	}

	public String check(String loginId) {
		Connection conn = null;

		try {

			conn = DatabaseManager.getConnection();

			String check = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement checkpStmt = conn.prepareStatement(check);
			checkpStmt.setString(1, loginId);
			ResultSet checkrs = checkpStmt.executeQuery();
			if (!checkrs.next()) {

				return null;

			}
			String result = checkrs.getString("login_id");
			System.out.println(result);
			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public int insert(String loginId, String Password, String Name, String birthdate) {
		Connection conn = null;

		try {

			conn = DatabaseManager.getConnection();

			String insertSql = "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date) VALUES(?, ?, ?, ?, now(), now())";
			PreparedStatement pStmt = conn.prepareStatement(insertSql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, Password);
			pStmt.setString(3, Name);
			pStmt.setString(4, birthdate);

			int rs = pStmt.executeUpdate();
			System.out.println("true");
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("fault");
			return 0;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	public int updateWithPass(String updateName, String updatePass, Date updateBirthDate, String id) {
		Connection conn = null;

		try {

			conn = DatabaseManager.getConnection();

			String updateSql = "UPDATE user SET name = ?, password = ?, birth_date = ? WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(updateSql);

			pStmt.setString(1, updateName);
			pStmt.setString(2, updatePass);
			pStmt.setDate(3, updateBirthDate);
			pStmt.setString(4, id);
			int result = pStmt.executeUpdate();
			return result;
		} catch (

		SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public void updateWithoutPass(String updateName, Date updateBirthDate, String id) {
		Connection conn = null;

		try {

			conn = DatabaseManager.getConnection();

			String updateSql = "UPDATE user SET name = ?, birth_date = ? WHERE id = ?";
			PreparedStatement pStmt = conn.prepareStatement(updateSql);

			pStmt.setString(1, updateName);
			pStmt.setDate(2, updateBirthDate);
			pStmt.setString(3, id);
			int result = pStmt.executeUpdate();

		} catch (

		SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public User delete(String Id) {
		Connection conn = null;

		try {
			conn = DatabaseManager.getConnection();
			String deleteSql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(deleteSql);
			pStmt.setString(1, Id);
			int rs = pStmt.executeUpdate();
			System.out.println(rs);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

}