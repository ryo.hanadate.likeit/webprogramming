package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.User;
import dao.UserDao;

@WebServlet("/UserListServlet")

public class UserListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public UserListServlet() {

		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		System.out.println(session);
		if (user == null) {
			response.sendRedirect("Login");
			return;
		}

		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_chart.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String loginId = request.getParameter("login-id");

		String name = request.getParameter("user-name");

		String dateStart = request.getParameter("date-start");
		// 本来であればjava.sql.dateでDate型に変換

		String dateEnd = request.getParameter("date-end");

		UserDao user = new UserDao();
		List<User> userList = user.search(loginId, name, dateStart, dateEnd);

		request.setAttribute("userList", userList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_chart.jsp");
		dispatcher.forward(request, response);
	}
}
