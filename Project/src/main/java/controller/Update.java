
package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Converter;
import Model.User;
import dao.UserDao;

/**
 * Servlet implementation class Update
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Update() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		System.out.println(session);
		if (user == null) {
			response.sendRedirect("Login");
			return;
		}

		String Id = request.getParameter("id");
		System.out.println(Id);
		UserDao userDao = new UserDao();
		User user2 = userDao.infomation(Id);
		request.setAttribute("userDetails", user2);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String updateName = request.getParameter("updateName");
		String updatePassword = request.getParameter("updatePassword");
		String updatePasswordCheck = request.getParameter("updatePasswordCheck");
		String birthDate = request.getParameter("updateBirthDate");
		System.out.println(birthDate);// 生年月日の確認用
		String Id = request.getParameter("id");

		UserDao userDao = new UserDao();
		Converter converter = new Converter();

		if (!updatePassword.equals(updatePasswordCheck) || updateName.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			System.out.println("n");
			String[] insertName = request.getParameterValues("updateName");
			String[] insertBirthDate = request.getParameterValues("updateBirthDate");
			String[] userId = request.getParameterValues("id");
			request.setAttribute("insertName", insertName);
			request.setAttribute("insertBirthDate", insertBirthDate);
			request.setAttribute("insertId", userId);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		Date updateBirthDate = java.sql.Date.valueOf(birthDate);

		if (!updatePassword.equals("")) {
			userDao.updateWithPass(updateName, converter.source(updatePassword), updateBirthDate, Id);
			System.out.println(1);
		}
		if (updatePassword.equals("")) {
			userDao.updateWithoutPass(updateName, updateBirthDate, Id);
			System.out.println(2);
		}
		response.sendRedirect("UserListServlet");
	}
}
