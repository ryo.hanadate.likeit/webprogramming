package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Converter;
import Model.User;
import dao.UserDao;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/Signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Signup() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		System.out.println(session);
		if (user == null) {
			response.sendRedirect("Login");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_signup.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String inputloginId = request.getParameter("inputloginId");
		String inputPassword = request.getParameter("inputPassword");
		String inputPasswordCheck = request.getParameter("inputPasswordCheck");
		String inputName = request.getParameter("inputName");
		String inputBirthDate = request.getParameter("inputBirthDate");

		UserDao userCheck = new UserDao();
		String user = userCheck.check(inputloginId);
		Converter converter = new Converter();
		UserDao userDao = new UserDao();

		if (inputloginId.equals(user)) {
			request.setAttribute("errMsg", "このIDは既に存在しています。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_signup.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if (inputloginId.equals("") || inputPassword.equals("") || inputPasswordCheck.equals("") || inputName.equals("")
				|| inputBirthDate.equals("")) {
			request.setAttribute("errMsg", "入力した内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_signup.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if (!inputPassword.equals(inputPasswordCheck)) {

			request.setAttribute("errMsg", "入力されたパスワードが一致しません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_signup.jsp");
			dispatcher.forward(request, response);

			return;
		}
		userDao.insert(inputloginId, converter.source(inputPassword), inputName, inputBirthDate);
		response.sendRedirect("UserListServlet");
	}

}
