package Model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Converter implements Serializable {
	public String source(String cryptography) {
		String source = cryptography;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		try {

			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			return result;
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
			return null;
		}
	}
}